/*
Nama : David Aditya Susanto
NPM : 140810190067
kelas : A
tanggal : 12 Mart 2020
*/

#include <iostream>
using namespace std;

struct hewan{
		
		string nama;
		int kode;
		int jumlah;
};

int main(){
		
		struct hewan hwn = {"kambing", 01, 12}; //memasukan elemen dari struct
		struct hewan *ptr = &hwn; //pointer dari struct 

		cout << ptr -> nama << endl; //memanggil nama dari struct
		cout << ptr ->kode << endl; //memanggil kode
		cout << ptr -> jumlah << endl; //memanggil jumlah
		
}

